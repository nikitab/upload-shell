#!/usr/bin/python -u

import os

import sys
import re
import time
import tempfile

BUFSIZE=8192

client_ip = os.environ['SSH_CLIENT'].split()[0]

if not re.match(r'((\d+)\.){3}\d+$', client_ip):
    sys.exit(1)

if not os.path.exists(client_ip):
    os.mkdir(client_ip)

filename = client_ip + '/' + re.sub(r'[^A-Za-z0-9-_.]+', '', sys.argv[2]) + \
    '--' + str(time.time())

f = tempfile.NamedTemporaryFile(mode='w+b', dir="./tmp", delete=False)
while True:
    data = sys.stdin.read(BUFSIZE)
    if not data:
        break
    f.write(data)
f.close()
os.rename(f.name, filename)
os.chmod(filename, 0o644)
