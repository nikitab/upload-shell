This can be used as a shell of a user that is intended to receive uploads. The approach is to use:

    cat filename | ssh user@host filename

The `filename` is saved into a directory corresponding to the IP address of the client, with a timestamp appended.

To install, you need to set `upload-shell.py` as the shell of the user to receive the uploads. If you also want the uploads to proceed automatically, you will want to either:

1. Generate an SSH public/private key pair and distribute the private key to those who need to upload, or
2. Set the user account to have no password.

If you select option 2, you will also need to enable logins without a password. To enable this on Ubuntu (tested under 12.04 and 14.04), I had to:

1. Edit `/etc/ssh/sshd_config` and set `PermitEmptyPassword yes`
2. Edit `/etc/pam.d/common-auth` and add `nullok` to the `pam_unix.so` line
